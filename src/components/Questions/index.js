import React from 'react';
import './questions.css';
import { useDispatch, useSelector } from 'react-redux';
import QButton from './QuestionButton';
import { Link } from 'react-router-dom';
import constants from '../../constants';

export default () => {
  const { active, activeSub, activeQuestion } = useSelector((state) => state.categories);
  const questions = useSelector((state) => state.questions);

  const dispatch = useDispatch();

  const buildComponents = (quest) => {
    if (!quest) {
      return;
    }
    let components = [];
    quest.forEach((question) => {
      components.push(< QButton question = {question}/>);
    });
    return components;
  };

  const onClick = (correct, answTeam) => {
    dispatch({
        type: constants.ANSWER_QUESTION,
        payload: {
          subcategory: activeSub || active,
          id: activeQuestion,
          correct,
          answTeam,
        },
      }
    );
  };

  const question = questions[activeSub || active].find((q) => q.id === activeQuestion);

  return (
    <div className='questions'>
      {activeQuestion &&
        <div className={'question'}>
          <span>{question.text}</span>
          <div className='buttonsContainer'>
          <Link to={'/'} className='ok' onClick={
            () => {
              onClick(true, 0);
            }
          }>
            <img src={process.env.PUBLIC_URL + '/img/' + 'player1' + '.png'} alt={'yes'}/>
          </Link>
            <Link to={'/'} className='ok' onClick={
              () => {
                onClick(true, 1);
              }
            }>
              <img src={process.env.PUBLIC_URL + '/img/' + 'player2' + '.png'} alt={'yes'}/>
            </Link>
            <Link to={'/'} className='ok' onClick={
              () => {
                onClick(true, 2);
              }
            }>
              <img src={process.env.PUBLIC_URL + '/img/' + 'player3' + '.png'} alt={'yes'}/>
            </Link>
          <Link to={'/'} className='no' onClick={
            () => {
              onClick(false);
            }
          }>
            <img src={process.env.PUBLIC_URL + '/img/' + 'no' + '.png'} alt={'no'}/>
          </Link>
            </div>
        </div>}
      {!activeQuestion && buildComponents(questions[activeSub || active])}
    </div>
  );
};
