import React from 'react';
import './button.css';
import { useDispatch } from 'react-redux';
import constants from '../../../constants';

export default (props) => {
  const { question } = props;
  const dispatch = useDispatch();

  const onClick = () => {
    dispatch({
      type: constants.SET_ACTIVE_QUESTION,
      payload: question.id,
    });
  };

  let className = question.points === 1 ? 'qbutton green' : 'qbutton';
  className = question.points === 2 ? 'qbutton yellow' : className;
  className = question.points === 3 ? 'qbutton red' : className;
  className = question.answered ? 'qbutton answered' : className;

  return (
    <button className={className} onClick={onClick} disabled={question.answered}>
      <div className={'buttonContent'}>
      <span>{question.name}</span>
        </div>
    </button>
  );
};
