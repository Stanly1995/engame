import React from 'react';
import Category from './SubCategory';
import './subcategories.css';
import { useSelector } from 'react-redux';

export default () => {
  const { active, activeSub } = useSelector((state) => state.categories);
  const categories = useSelector((state) => state.categories.categories);
  const questions = useSelector((state) => state.questions);
  console.log(active);
  const buildComponents = (c) => {
    if (!active) {
return;
}
    let components = [];
    c[active].forEach((name) => {
      let disabled = false;
      const elem = questions[name].find((q) => q.answered === false);
      if (!elem) {
disabled = true;
}
      components.push(<Category
        name={name}
        image={process.env.PUBLIC_URL + '/img/' + name + '.png'}
        path={'/' + name}
        disabled={disabled}
      />);
    });
    return components;
  };
  return (
    <div className='categories'>
      {buildComponents(categories)}
    </div>
  );
};
