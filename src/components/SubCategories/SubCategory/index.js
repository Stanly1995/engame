import React from 'react';
import './subcategory.css';
import {
  Link,
} from 'react-router-dom';
import { useDispatch } from 'react-redux';
import constants from '../../../constants';

export default (props) => {
  const { name, image, path, disabled } = props;
  const dispatch = useDispatch();

  const onCategoryClick = () => {
    dispatch({
      type: constants.SET_ACTIVE_SUBCATEGORY,
      payload: name,
    });
  };
  const className = disabled ? 'subcategory disabled' : 'subcategory';
  return (
    <button className={className}>
    <Link path={'/' + name} to={path} onClick={
      () => {
        onCategoryClick();
      }
    }
          disabled={disabled}
    >
      <img src={image} alt={'category'}/>
    </Link>
      </button>
  );
};
