import React from 'react';
import './team.css';

export default (props) => {
  const { team, image } = props;
  return (
    <div className='team'>
      <img src={image} alt={'team'}/>
      <span>{team.points}</span>
    </div>
  );
};
