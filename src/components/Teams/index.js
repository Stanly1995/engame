import React from 'react';
import Team from './Team';
import './teams.css';
import { useSelector } from 'react-redux';

export default () => {
  const teams = useSelector((state) => [...state.teams.teams]);
  return (
    <div className='teams'>
      <Team image={process.env.PUBLIC_URL + '/img/player1.png'} team={teams[0]}/>
      <Team image={process.env.PUBLIC_URL + '/img/player2.png'} team={teams[1]}/>
      <Team image={process.env.PUBLIC_URL + '/img/player3.png'} team={teams[2]}/>
    </div>
  );
};
