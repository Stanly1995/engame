import React from 'react';
import './button.css';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import constants from '../../constants';

export default () => {
  const dispatch = useDispatch();
  const { activeQuestion } = useSelector((state) => state.categories);

  const onClick = () => {
    dispatch({
      type: constants.SET_ACTIVE_QUESTION,
      payload: '',
    });
    dispatch({
      type: constants.SET_ACTIVE_SUBCATEGORY,
      payload: '',
    });
    dispatch({
      type: constants.SET_ACTIVE_CATEGORY,
      payload: '',
    });
  };

  const className = 'back-button';

  return (
  !activeQuestion && <div className={className}>
      <Link className={'buttonContent'} onClick={onClick} to={'/'}>
        <img src={process.env.PUBLIC_URL + '/img/' + 'back' + '.png'} alt={'back'}/>
      </Link>
    </div>
  );
};
