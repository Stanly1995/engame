import React from 'react';
import { useSelector } from 'react-redux';
import Category from './Category';
import './categories.css';

export default () => {
  const categories = useSelector((state) => state.categories.categories);
  console.log(categories);

  const buildComponents = (c) => {
    let components = [];
    Object.keys(c).forEach((name) => {
      components.push(<Category
        name={name}
        image={process.env.PUBLIC_URL + '/img/' + name + '.png'}
        path={'/' + name}
        />);
    });
    return components;
  };
  return (
    <div className='categories'>
      {buildComponents(categories)}
    </div>
  );
};
