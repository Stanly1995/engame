import React from 'react';
import './category.css';
import {
  Link,
} from 'react-router-dom';
import constants from '../../../constants';
import { useDispatch } from 'react-redux';

export default (props) => {
  const { name, image, path } = props;
  const dispatch = useDispatch();

  const onCategoryClick = () => {
    dispatch({
      type: constants.SET_ACTIVE_CATEGORY,
      payload: name,
    });
  };

  return (
    <Link to={path} className='category' onClick={
      () => {
onCategoryClick();
}
    }>
      <img src={image} alt={'category'}/>
    </Link>
  );
};
