import constants from '../constants';

const initialState = {
  active: '',
  activeSub: '',
  activeQuestion: 0,
  categories: {
    sherlock: ['rip', 'stolen'],
    queen: ['toeng', 'fromeng'],
    cab: [],
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.SET_ACTIVE_CATEGORY:
      return { ...state, active: action.payload };
    case constants.SET_ACTIVE_SUBCATEGORY:
      return { ...state, activeSub: action.payload };
    case constants.SET_ACTIVE_QUESTION:
      return { ...state, activeQuestion: action.payload };
    default:
      return state;
  }
};
