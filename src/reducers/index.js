import { combineReducers } from 'redux';
import categories from './categories';
import questions from './questions';
import teams from './teams';

export default combineReducers({
  teams,
  categories,
  questions,
});
