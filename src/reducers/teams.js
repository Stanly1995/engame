import constants from '../constants';

const initialState = {
  active: 0,
  teams: [
    {
      points: 0,
    },
    {
      points: 0,
    },
    {
      points: 0,
    },
  ],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case constants.SET_TEAMS:
      return { ...action.payload };

    default:
      return state;
  }
};
