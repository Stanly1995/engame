import { all, takeEvery } from 'redux-saga/effects';
import constants from '../constants';
import answerQuestion from './answerQuestion';

export default function* rootSaga() {
  yield all([
    yield takeEvery(constants.ANSWER_QUESTION, answerQuestion),
  ]);
}
