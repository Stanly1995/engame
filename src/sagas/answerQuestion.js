import { select, put } from 'redux-saga/effects';
import constants from '../constants';

export default function* answerQuestion(a) {
  const { subcategory, id, correct, answTeam } = a.payload;
  const questions = yield select((state) => state.questions);
  const sub = questions[subcategory];
  const { active, teams } = yield select((state) => state.teams);
  const team = teams[answTeam];
  let points = 0;
  sub.forEach((quest) => {
    if (quest.id === id) {
      quest.answered = true;
      points = quest.points;
    }
  });
  if (team) {
  teams[answTeam].points = correct ? team.points + points : team.points;
  }
  // const newActive = active === teams.length - 1 ? 0 : active + 1;
  yield put({
    type: constants.SET_QUESTIONS,
    payload: questions,
  });
  yield put({
    type: constants.SET_TEAMS,
    payload: {
      active,
      teams,
    },
  });
  yield put({
    type: constants.SET_ACTIVE_CATEGORY,
    payload: '',
  });
  yield put({
    type: constants.SET_ACTIVE_SUBCATEGORY,
    payload: '',
  });
  yield put({
    type: constants.SET_ACTIVE_QUESTION,
    payload: '',
  });
}
