const constants = {};

Object.defineProperties(constants, {
  SET_ACTIVE_CATEGORY: { value: 'SET_ACTIVE_CATEGORY', writable: false },
  SET_ACTIVE_SUBCATEGORY: { value: 'SET_ACTIVE_SUBCATEGORY', writable: false },
  SET_ACTIVE_QUESTION: { value: 'SET_ACTIVE_QUESTION', writable: false },
  SET_TEAMS: { value: 'SET_TEAMS', writable: false },
  ANSWER_QUESTION: { value: 'ANSWER_QUESTION', writable: false },
  SET_QUESTIONS: { value: 'SET_QUESTIONS', writable: false },
});

export default constants;
