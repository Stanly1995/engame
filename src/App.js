import React from 'react';
import Categories from './components/Categories';
import SubCategories from './components/SubCategories';
import Questions from './components/Questions';
import Teams from './components/Teams';
import BackBtn from './components/BackBtn';
import './App.css';
import {
  BrowserRouter,
  Switch,
  Route,
} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
    <div className='app'>
      <BackBtn />
      <Switch>
        <Route exact path='/'>
          <Categories />
        </Route>
        <Route exact path='/sherlock'>
          <SubCategories />
        </Route>
        <Route exact path='/queen'>
          <SubCategories />
        </Route>
        <Route exact path='/cab'>
          <Questions />
        </Route>
        <Route exact path='/rip'>
          <Questions />
        </Route>
        <Route exact path='/stolen'>
          <Questions />
        </Route>
        <Route exact path='/fromeng'>
          <Questions />
        </Route>
        <Route exact path='/toeng'>
          <Questions />
        </Route>
      </Switch>
      <Teams />
    </div>
    </BrowserRouter>
  );
}

export default App;
